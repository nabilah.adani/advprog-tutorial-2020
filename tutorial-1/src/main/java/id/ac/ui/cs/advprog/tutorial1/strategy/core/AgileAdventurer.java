package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AgileAdventurer extends Adventurer {
    public AgileAdventurer(AttackBehavior attackBehavior, DefenseBehavior defenseBehavior){
        this.setAttackBehavior(attackBehavior);
        this.setDefenseBehavior(defenseBehavior);
    }
    public AgileAdventurer(){
        this(new AttackWithGun(), new DefendWithBarrier());
    }
    public String getAlias(){
        return "AgileAdventurer";
    }

}
