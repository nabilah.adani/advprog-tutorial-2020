package id.ac.ui.cs.advprog.tutorial1.observer.service;

import id.ac.ui.cs.advprog.tutorial1.observer.core.*;
import id.ac.ui.cs.advprog.tutorial1.observer.repository.QuestRepository;
import id.ac.ui.cs.advprog.tutorial1.observer.core.Adventurer;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.ArrayList;

@Service
public class GuildServiceImpl implements GuildService {
        private final QuestRepository questRepository;
        private final Guild guild;
        private final Adventurer agileAdventurer;
        private final Adventurer knightAdventurer;
        private final Adventurer mysticAdventurer;

        public GuildServiceImpl(QuestRepository questRepository) {
                this.questRepository = questRepository;
                this.guild = new Guild();
                this.agileAdventurer = new AgileAdventurer(guild);
                this.knightAdventurer = new KnightAdventurer(guild);
                this.mysticAdventurer = new MysticAdventurer(guild);
                this.guild.add(this.agileAdventurer);
                this.guild.add(this.knightAdventurer);
                this.guild.add(this.mysticAdventurer);
        }
        public void addQuest(Quest quest) {
                Quest curQuest = this.questRepository.save(quest);
                if (curQuest != null) {
                        this.guild.addQuest(quest);
                }
        }

        public List<Adventurer> getAdventurers() {
                List<Adventurer> result = new ArrayList<>();
                result.add(this.agileAdventurer);
                result.add(this.knightAdventurer);
                result.add(this.mysticAdventurer);
                return result;
        }


}
