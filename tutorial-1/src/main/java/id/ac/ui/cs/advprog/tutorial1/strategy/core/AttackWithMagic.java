package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    public AttackWithMagic(){}

    @Override
    public String attack() {
        return "Attack with Magic!";
    }

    @Override
    public String getType() {
        return "Magic";
    }
}
