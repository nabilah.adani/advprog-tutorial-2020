package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

        public AgileAdventurer(Guild guild) {
                this.name = "Agile";
                this.guild = guild;
        }

        @Override
        public void update(){
                if(guild.getQuestType().equals("De") ||
                        guild.getQuestType().equals("Ru")) {
                        this.getQuests().add(guild.getQuest());
                }
        }

}
