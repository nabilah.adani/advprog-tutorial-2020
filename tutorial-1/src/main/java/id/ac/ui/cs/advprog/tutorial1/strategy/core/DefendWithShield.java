package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
    public DefendWithShield(){}

    @Override
    public String defend() {
        return "Defend With Shield!";
    }

    @Override
    public String getType() {
        return "Shield";
    }
}
