package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {
    public KnightAdventurer(AttackBehavior attackBehavior, DefenseBehavior defenseBehavior){
        this.setAttackBehavior(attackBehavior);
        this.setDefenseBehavior(defenseBehavior);
    }

    public KnightAdventurer(){
        this(new AttackWithSword(), new DefendWithArmor());
    }

    @Override
    public String getAlias() {
        return "KnightAdventurer";
    }
}
