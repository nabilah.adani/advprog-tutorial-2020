package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {
    public MysticAdventurer(AttackBehavior attackBehavior, DefenseBehavior defenseBehavior){
        this.setAttackBehavior(attackBehavior);
        this.setDefenseBehavior(defenseBehavior);
    }

    public MysticAdventurer(){
        this(new AttackWithMagic(), new DefendWithShield());
    }

    @Override
    public String getAlias() {
        return "MysticAdventurer";
    }
}
